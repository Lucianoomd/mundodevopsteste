import React from 'react';
import { View } from 'react-native';
import Navigator from './src/navigation/Navigator';

export default App = () => (
    <View style={styles.container}>
        <Navigator />
    </View>
);

const styles = {
  container: {
    flex: 1
  }
};