Este projeto tem como intuito representar o design do do arquivo sketch.png.
Foi implementado uma lista de exercícios e uma lista de filtros, quando um item da lista
de filtros é selecionado a lista de exercícios é atualizada e apresenta somente os exercícios
que existem com o filtro selecionado.
Não foi implementado nenhuma funcionalidade relacionada aos botões direito e esquerdo do header.
Não foi implementado o gradiente do background do item de filtro.

OBS: O projeto foi desenvolvido e testado para iOS, não foi testado em android.

-----------------------------------------------------

Siga os passos abaixo para executar o projeto.
OBS: O Android studio e pelo menos um emulador deve estar configurado.
    Ou o Xcode(somente para MAC) instalado e preparado para usar o Simulador.


1 - Instale o gerenciador de dependencias nodejs
    (pode ser baixado em https://nodejs.org/en/download/)

2 - Abra o terminal(MAC ou Linux) ou prompt de comando(Windows)

3 - Instale a dependencia do react native com o comando:
    npm install -g react-native-cli

4 - Abra a pasta do projeto com o comando "cd" seguido do local 
    onde o projeto está, por exemplo: "cd caminho/do/projeto"

5 - Execute o comando "npm install" para instalar as dependencias
    utilizadas no projeto

6a - Para executar o projeto no emulador do android o comando:
    react-native run-android
6b - Para executar o projeto no simulador do iOS utilize o comando:
    react-native run-ios