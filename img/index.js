const Images = {
    filters: {
        'Yoga': require('./ic_yoga.png'),
        'Upper Body': require('./ic_upper_body.png'),
        'Lower Body': require('./ic_lower_body.png'),
        'Dance': require('./ic_dance.png')
    },
    exercices: {
        'Corrida': require('./running.png'),
        'Bicicleta': require('./cycling.png'),
        'Musculação': require('./gym.png'),
        'Yoga': require('./yoga.png'),
    },
    icons: {
        'calories': require('./ic_bike.png'),
        'time': require('./ic_time.png'),
        'weight': require('./ic_balance.png')
    }
};

export default Images;