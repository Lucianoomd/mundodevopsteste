import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Colors from '../constants/Colors';
import CustomText from '../components/CustomText';
import Icon from 'react-native-vector-icons/FontAwesome';
import FilterList from '../components/FilterList';
import DataUtils from '../utils/DataUtils';
import Spinner from '../components/Spinner';
import ExerciceList from '../components/ExerciceList';


export default class Home extends React.Component {
    static navigationOptions = {
        headerTitle: <CustomText text='Meu Perfil' upperCase fontSize={20} />,
        headerRight: (
            <TouchableOpacity 
            style={{paddingRight: 40}}
            onPress={this.handleHeaderRight}
            >
                <Icon name="sun-o" size={20} color={Colors.white} />
            </TouchableOpacity>
        ),
        headerLeft: (
            <TouchableOpacity 
            style={{paddingLeft: 40}} 
            onPress={this.handleHeaderRight}
            >
                <Icon name="bars" size={20} color={Colors.white} />
            </TouchableOpacity>
        ),
        headerStyle: {
            backgroundColor: Colors.darkGray,
            marginBottom: 20,
            borderBottomWidth: 0
        }
    };

    constructor(props){
        super(props);
        this.state = { filters: [], exercices: [], filteredExercices: [], loading: true };
    }

    componentWillMount(){
        const { state } = this;
        let newState = {...state, ...DataUtils.getLocalData()};
        newState.loading = false;
        this.setState(newState);
    }

    handleFilter = filterIndex => {
        this.setState({loading: true}, () => {
            this.selectOrRemoveFilter(filterIndex);
        });
    }

    selectOrRemoveFilter(index) {
        let newFilters = this.state.filters;
        newFilters[index].selected = !newFilters[index].selected;
        let filteredExercices = this.applyOrRemoveFilter(newFilters);

        this.setState({
            filters: newFilters,
            filteredExercices, 
            loading: false
        });
    }

    isFilterActive(filterName, newFilters){
        for (let i = 0; i < newFilters.length; i++) {
            const f = newFilters[i];
            if(f.name === filterName && f.selected) return true;
        }
        return false;
    }

    applyOrRemoveFilter = (newFilters) => {
        const { exercices } = this.state;

        let filteredExercices = [];
        
        for (let i = 0; i < exercices.length; i++) {
            const exercice = exercices[i];
            if(this.isFilterActive(exercice.name, newFilters)) filteredExercices.push(exercice);
        }

        return filteredExercices;
    }

    isAnyFilterActive() {
        const { filters } = this.state;
        let count = 0;

        filters.forEach(filter => {
            if(filter.selected) count++;
        });

        return count;
    }

    render() {
        const { filters, exercices, loading, filteredExercices } = this.state;

        if(loading){
            return <Spinner />
        }

        return(
        <View style={styles.container}>
            <View style={styles.paddingsContainer}>
                <View style={styles.contentContainer} />
            </View>
            <View>
                <FilterList filters={filters} handleFilter={this.handleFilter} />
                {this.isAnyFilterActive() && !filteredExercices.length ? 
                <View style={styles.paddingsContainer}>
                    <CustomText text='Nenhum exercício encontrado.'  />
                </View>
                : <ExerciceList exercices={exercices}
                filteredExercices={filteredExercices} />}
            </View>
        </View>
        );
    }
}

const styles = {
    container: {
        backgroundColor: Colors.darkGray,
        flex: 1
    },
    contentContainer: {
        borderTopWidth: 0.3,
        borderColor: Colors.white,
        paddingTop: 12,
        paddingBottom: 20
    },
    paddingsContainer: {
        paddingLeft: 40,
        paddingRight: 40
    }
};

