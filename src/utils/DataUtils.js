

export default class DataUtils {
    static getLocalData(){
        let customReturn = {};
        let data = require('../../data/data.json');
        
        if(data){
            customReturn.filters = data.filters;
            customReturn.exercices = data.exercices;
        }
        else
            customReturn.error = 'Error reading data. Try again later.';
        
        return customReturn;
    }

}