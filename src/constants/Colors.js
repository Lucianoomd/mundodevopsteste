export default Colors = {
    white: '#FEFFFF',
    transparentWhite: '#FEFFFF99',
    purple: '#7F38F4',
    pink: '#F22B48',
    green: '#19B996',
    orange: '#FD3C29',
    darkGray: '#262F38',
    gray: '#323C47'
}