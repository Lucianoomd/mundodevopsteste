import React from 'react';
import { Text } from 'react-native';
import Colors from '../constants/Colors';

const styles = {
  defaultText: {
    color: Colors.white,
    fontSize: 14,
    marginBottom: 4,
    fontFamily: 'Montserrat-Regular'
  }
};

const CustomText = ({ text, textColor, fontSize, upperCase, bold, lineHeight, textAlign }) => {
  const newStyle = { ...styles.defaultText };

  if(fontSize) newStyle.fontSize = fontSize;
  

  if (textColor) newStyle.color = textColor;
  

  if(bold) newStyle.fontFamily = 'MontSerrat-Bold';
  

  if(lineHeight) newStyle.lineHeight = lineHeight;

  if(textAlign) newStyle.textAlign = textAlign;
  
  return <Text style={newStyle}>{upperCase ? text.toUpperCase() : text}</Text>;
};

export default CustomText;
