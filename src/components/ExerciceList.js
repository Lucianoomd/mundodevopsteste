import React from 'react';
import { FlatList } from 'react-native';
import ExerciceItem from './ExerciceItem';

const styles = {
    listContainer: {
        paddingTop: 30
    }
};

const ExerciceList = ({ exercices, filteredExercices }) => {
  const keyExtractor = item => item.name;
  return !exercices || exercices.length < 1 ? null : (
      <FlatList
        style={styles.listContainer}
        contentContainerStyle={{paddingBottom: 50}}
        data={filteredExercices.length ? filteredExercices : exercices}
        renderItem={({ item }) => 
          <ExerciceItem exercice={item} />
        }
        keyExtractor={keyExtractor}
      />
  );
};

export default ExerciceList;
