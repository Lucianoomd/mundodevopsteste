import React from 'react';
import { View, TouchableOpacity, Image } from 'react-native';
import Colors from '../constants/Colors';
import Icon from 'react-native-vector-icons/FontAwesome';
import Images from '../../img';

const styles = {
  container: {
    borderRadius: 25,
    backgroundColor: Colors.pink,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    width: 50
  },
  checkContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
    borderRadius: 9,
    backgroundColor: Colors.green,
    justifyContent: 'center',
    alignItems: 'center',
    height: 18,
    width: 18,
    borderWidth: 2,
    borderColor: Colors.white
  }
};

const FilterItem = ({filter, handleFilter, filterIndex}) => {
  const callback = () => {
    handleFilter(filterIndex);
  }

  return (
    <TouchableOpacity style={styles.container} onPress={callback} >
        <Image source={Images.filters[filter.name]} style={{ maxHeight: 30, maxWidth: 30 }} />
        {filter.selected ? <View style={styles.checkContainer}>
            <Icon name="check" size={11} color={Colors.white} />
        </View>
        : null}
    </TouchableOpacity>
  );
}

export default FilterItem;