import React from 'react';
import { View, Image } from 'react-native';
import Colors from '../constants/Colors';
import Images from '../../img';
import CustomText from './CustomText';

const styles = {
    container: {
        paddingLeft: 40,
        paddingRight: 40
    },
    itemContainer: {
        borderRadius: 20,
        backgroundColor: Colors.gray,
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 16,
        paddingBottom: 12,
        margintTop: 30,
        marginBottom: 30
    },
    row: {
        flexDirection: 'row'
    },
    imageContainer: {
        borderRadius: 40,
        height: 80,
        width: 80,
        marginRight: 30,
        backgroundColor: Colors.darkGray,
        justifyContent: 'center',
        alignItems: 'center'
    },
    infoContainer: {
        justifyContent: 'center'
    },
    infoImageItem: {
        height: 20
    },
    borderRight: {
        backgroundColor: Colors.transparentWhite,
        height: 10,
        width: 0.4,
        marginRight: 4,
        marginLeft: 2
    },
    emptyDayContainer: {
        marginTop: 4,
        marginRight: 8,
        paddingLeft: 12,
        paddingRight: 12,
        borderRadius: 8,
        borderWidth: 1,
        borderColor: Colors.transparentWhite
    },
    yesterdayContainer: {
        borderWidth: 0,
        backgroundColor: Colors.green
    },
    todayContainer: {
        borderWidth: 0,
        backgroundColor: Colors.orange
    }

};

const ExerciceItem = ({ exercice }) => {
    const { name, calories, time, weight, when } = exercice;
    const infoItem = (borderRight, value, type) => {
        let measureText = value.toString();
        switch (type) {
            case 'calories':
                measureText += ' Kcal';
                break;
            case 'weight':
                measureText += ' Kg';
                break;
            default:
                if(value % 60 == 0) measureText = `${value / 60} h`;
                else measureText += ' m';
        }
        return(
            <View style={{...styles.row, ...{alignItems: 'center', justifyContent: 'center'}}}>  
                <Image source={Images.icons[type]} style={{ maxHeight: 15 }} />
                <View style={{marginLeft: 4, marginRight: 4 }}>
                    <CustomText text={measureText} fontSize={8} bold />
                </View>
                <View style={borderRight ? styles.borderRight : null} />
            </View>
        );
    }
    const dayItem = (value, today) => {
        let dayItemStyle = styles.emptyDayContainer;
        const dayText = today ? 'Hoje' : 'Ontem';
        const {todayContainer, yesterdayContainer} = styles;

        if(value == 'today' && today) dayItemStyle = {...dayItemStyle, ...todayContainer};
        else if (value == 'yesterday' && !today) dayItemStyle = {...dayItemStyle, ...yesterdayContainer};

        return <View style={dayItemStyle}>
                    <CustomText text={dayText} upperCase fontSize={7} textAlign='center'/>
                </View>
    }   
        
    return (
        <View style={styles.container}>
            <View style={styles.itemContainer}>
                <View style={styles.row}>
                    <View style={styles.imageContainer}>
                        <Image source={Images.exercices[name]} />
                    </View>
                    <View style={styles.infoContainer}>
                        <CustomText text={name} fontSize={16} upperCase bold />
                        <View style={styles.row}>
                            {infoItem(true, calories, 'calories')}
                            {infoItem(true, time, 'time')}
                            {infoItem(false, weight, 'weight')}
                        </View>
                        <View style={styles.row}>
                            {dayItem(when, 1)}
                            {dayItem(when)}
                        </View>
                    </View>
                </View>
            </View>
        </View>
    );
}

export default ExerciceItem;