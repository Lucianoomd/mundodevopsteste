import React from 'react';
import { View, FlatList } from 'react-native';
import Colors from '../constants/Colors';
import FilterItem from './FilterItem';

const styles = {
    container: {
      paddingLeft: 40,
      paddingRight: 40
    },
    listContainer: {
        borderRadius: 20,
        backgroundColor: Colors.gray,
        paddingLeft: 30,
        paddingRight: 30,
        paddingTop: 16,
        paddingBottom: 12
    },
    separator: {
      marginRight: 12
    }
};

const FilterList = ({ filters, handleFilter }) => {
  const keyExtractor = (item, index) => {
    return (item.name + index);
  };

  return !filters || filters.length < 1 ? null : (
    <View style={styles.container}> 
      <FlatList
          style={styles.listContainer}
          contentContainerStyle={{paddingRight: 60}}
          data={filters}
          renderItem={({ item, index }) => 
              <FilterItem filter={item} handleFilter={handleFilter} filterIndex={index} />
          }
          keyExtractor={keyExtractor}
          ItemSeparatorComponent={() => 
            <View style={styles.separator} />
          }
          horizontal
        />
    </View>
  );
};

export default FilterList;
